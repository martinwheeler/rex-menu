This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

*It is currently still in an Alpha state.*

# Description

This repo is currently a proof-of-concept so we may begin architecting a complete solution to bring Rex 2 into React. Below you will find the results from the respective stages throughout this Alpha.

Current testing branch for Rex 2: `feature/rex-menu`

## Stage 1

### Goals

1. ~~Get an iFrame of Rex 2 loading inside of a create-react-app instance~~
2. ~~Figure out a way for the React app to control or update the iFrames current route~~
3. ~~Ensure we have a source of truth for the current route~~
4. Create a single location for controlling routing that either project can call upon

### Results

1. Injecting Rex into an iFrame required some modification to the X-Frame-Options header. This will need to be modified to `ALLOW-FROM uri`.
2. The iFrame's `src` property gets updated when ever Rex navigates to a new URL and we can use this to manipulate the iFrame's current view.
3. The iFrame's `src` will suffice as a source of truth for all frames and is easily referencable from either app.
   - Inside of the React app we can grab a reference to the iFrame element and use the `src` property.
   - From inside of Rex 2 we can use `window.self.location` to find where we are if required.
4. This has not been completed but should be fairly straight forward and will require exposing the router on a global variable so both apps can reference the new routing solution.

## Stage 2
    TBA