# Goals

1. Get an iframe of Rex 2 loading inside of a create-react-app instance
2. Figure out a way for the React app to control or update the iframes current route
3. Ensure we have a source of truth for the current route
4. Create a single location for controlling routing that either project can call upon

# General Notes

* Now know that window.top is a read only reference to the top-most window when dealing with multiple frames WHAT?!


## Globally used functions to route

1. r3.router.navigateToUrl ==> calls the below functions
2. r2.u.actions.openInNewTab
3. r2.u.actions.redirectToPage

# Initial Problems

* Needed to move my dev environment away from valet to use Apache for Reverse Proxying
* Problems trying to get php71 working with Apache config
* Brew decided to delete a required directory of MySQL while I was trying to get Apache & php71 to talk to each other
* I wrote some PHP to make sure Wings was calling the correct files: https://rexsoftware.slack.com/files/U509C975H/F73DHJLM9/screen_shot_2017-09-15_at_11.54.10_am.png
* Updated my local file permissions to 777 due to permission issues
* Enabled all of the required PHP modules from both Rex & Wings from the .htaccess files
* X-Frame-Options SAMEORIGIN does not allow subdomains will need to change to X-Frame-Options ALLOW-FROM uri