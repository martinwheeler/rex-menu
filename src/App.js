import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

const styles = {
  menuContainer: {
    height: "5rem",
    margin: 0,
    padding: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  iframe: {
    height: "90vh",
    margin: 0,
    padding: 0
  }
};

class Menu extends Component {
  render() {
    const { changeRoute } = this.props;
    return (
      <div style={styles.menuContainer}>
        <a onClick={changeRoute("/dashboard")}>Dashboard</a>
        <a onClick={changeRoute("/properties")}>Properties</a>
        <a onClick={changeRoute("/listings")}>Listings</a>
        <a onClick={changeRoute("/contacts")}>Contacts</a>
        <a onClick={changeRoute("/deals")}>Deals</a>
      </div>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRoute: "http://rex.dev/",
      isLoading: true
    };
  }

  componentDidMount() {
    this.iframe.addEventListener("load", this.shouldBeLoading(false));
  }

  shouldBeLoading = loadingState => () => {
    this.setState({ isLoading: loadingState });
  };

  updateCurrentRoute = newRoute => () => {
    this.setState(
      { currentRoute: `http://rex.dev${newRoute}/` },
      this.shouldBeLoading(true)
    );
  };

  render() {
    const { currentRoute, isLoading } = this.state;
    return (
      <div className="App" style={styles.iframe}>
        <Menu changeRoute={this.updateCurrentRoute} />
        {isLoading && <div>Loading ...</div>}
        <iframe
          title={"Rex"}
          frameBorder={0}
          src={currentRoute}
          style={{ height: "90vh", display: isLoading ? "none" : "block" }}
          width="100%"
          ref={iframe => (this.iframe = iframe)}
        />
      </div>
    );
  }
}

export default App;
